@echo off

::协议文件路径, 最后不要跟“\”符号
set SOURCE_FOLDER=proto_source

::C#编译器路径
set CS_COMPILER_PATH=tools\protoc.exe

::C#文件生成路径, 最后不要跟“\”符号
set CS_TARGET_PATH=..\Assets\GameCore\Protocol

::删除之前创建的文件
del %CS_TARGET_PATH%\*.* /f /s /q

set PROTOSUFFIX=*.proto

echo start generate csharp file to %CS_TARGET_PATH%
::遍历所有文件
for /f "delims=" %%i in ('dir /b %SOURCE_FOLDER%\%PROTOSUFFIX%') do (
    ::生成 C# 代码
	echo %CS_COMPILER_PATH% -I=%SOURCE_FOLDER% --csharp_out=%CS_TARGET_PATH% %%i
	%CS_COMPILER_PATH% -I=%SOURCE_FOLDER% --csharp_out=%CS_TARGET_PATH% %%i
)

echo gen over
pause


 