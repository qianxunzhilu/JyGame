﻿/*
 *  date: 2018-07-12
 *  author: John-chen
 *  cn: 客户端使用的资源管理器
 *  en: todo:
 */

using UnityEngine;
using JyFramework;
using System.Collections;
using Object = UnityEngine.Object;

namespace GameCore.BaseCore
{
    /// <summary>
    /// 客户端使用的资源管理器
    /// </summary>
    public class ResMgr : Singleton<ResMgr> , IResLoadFunction
    {
        private ResLoader _loader;
        private ResourceModule _resModule;

        protected override void Init()
        {
            base.Init();
            _resModule = GameApp.App.GetModule<ResourceModule>(ModuleName.Res);
            _loader = _resModule.Loader;

            Debug.Log("-----");
        }

        /// <summary>
        /// 同步加载 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public T LoadRes<T>(string path) where T : Object
        {
            return _loader.LoadRes<T>(path);
        }

        /// <summary>
        /// 异步返回加载资源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        public T LoadRes<T>(YieldArgs yieldArgs) where T : Object
        {
            return _loader.LoadRes<T>(yieldArgs);
        }

        /// <summary>
        /// 协程异步加载
        /// </summary>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        public IEnumerator AsyncLoadRes(YieldArgs yieldArgs, AsyncLoadedHandle action = null)
        {
            yield return _loader.AsyncLoadRes(yieldArgs, action);
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        public GameObject Instantiate(YieldArgs yieldArgs)
        {
            return _loader.Instantiate(yieldArgs);
        }

        /// <summary>
        /// 异步实例化
        /// </summary>
        /// <param name="yieldArgs">  </param>
        /// <param name="action"> 实例化成功后的回调 </param>
        /// <returns></returns>
        public IEnumerator AsyncInstantiate(YieldArgs yieldArgs, AsyncInstantiateHandler action)
        {
            yield return _loader.AsyncInstantiate(yieldArgs, action);
        }
    }
}