﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 事件接口
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 事件接口
    /// </summary>
    public interface IEvent
    {
        string Name { get; }
        void RegistEvent(string name, MessageHandler eventActioin);
        void NotifyEvent(string name, params object[] args);
        void RemoveEvent(string name, MessageHandler eventAction);
    }
}
