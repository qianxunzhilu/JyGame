﻿/*
 * date: 2018-10-09
 * author: John-chen
 * cn: 日志信息
 * en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 日志级别
    /// </summary>
    public enum LogLevel
    {
        Info,
        Warning,
        Error,
    }

    /// <summary>
    /// 日志打印代理
    /// </summary>
    /// <param name="content"></param>
    /// <param name="lv"></param>
    public delegate void LogDelegate(object content, LogLevel lv);
}