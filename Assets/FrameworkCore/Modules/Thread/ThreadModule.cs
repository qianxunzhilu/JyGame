﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 线程模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 线程模块
    /// </summary>
    public class ThreadModule : BaseModule
    {
        public ThreadModule(string name = ModuleName.Thread) : base(name)
        {
        }

        public override void InitGame()
        {
            
        }
    }
}
