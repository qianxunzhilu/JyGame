﻿/*
 *  date: 2018-08-19
 *  author: John-chen
 *  cn: UGUI 的 UIRoot的信息
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// UGUI 的根节点信息
    /// </summary>
    public class UGUIRootInfo : UIRootInfo
    {
        /// <summary>
        /// Canvas root节点
        /// </summary>
        public Canvas RootCanvas { get { return _rootCanvas;} }

        public UGUIRootInfo(GameObject rootObj, VoidDelegateSetWindow setWindowFunc) :base(rootObj, setWindowFunc)
        {
            _uiCamera = _rootTran.Find("UICamera")?.GetComponent<Camera>();
            _rootCanvas = _rootTran.Find("CanvasRoot")?.GetComponent<Canvas>();
        }

        protected Canvas _rootCanvas;
    }
}